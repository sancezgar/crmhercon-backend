<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//rutas Sucursal
Route::resource('api/sucursal','SucursalController');
//rutas Usuarios
Route::resource('api/user','UserController');
Route::post('api/login','UserController@login');
Route::post('api/revisarToken','UserController@revisarToken');
//rutas Sectores Economicos
Route::resource('api/sector','SectorEconomicoController');
//rutas Monedas
Route::resource('api/moneda','MonedaController');
//rutas TiposMaquinas
Route::resource('api/tipoMaquina','TipoMaquinaController');
//rutas Prospectos
Route::resource('api/prospecto','ProspectoController');
//rutas Contactos
Route::resource('api/contacto','ContactoController');
//rutas Interacciones
Route::resource('api/interaccion','InteraccionController');
//rutas Aditamentos
Route::resource('api/aditamento','AditamentoController');
//rutas Maquinas
Route::resource('api/maquina','MaquinaController');
//rutas Acciones
Route::resource('api/accion','AccionController');
