create DATABASE IF NOT EXISTS api_crm;

use api_crm;

CREATE TABLE sucursales(

  id_sucursal int auto_increment not null,
  razon_social text,
  rfc_sucursal text,
  alias text,
  created_at datetime default null,
  updated_at datetime default null,
  CONSTRAINT pk_sucursales PRIMARY KEY (id_sucursal)

) ENGINE=InnoDB;

CREATE TABLE users(

  id_usuario int auto_increment not null,
  nombre_usuario text,
  rol text,
  email_usuario text,
  contraseña text,
  id_sucursal int,
  created_at datetime default null,
  updated_at datetime default null,
  CONSTRAINT pk_users PRIMARY KEY (id_usuario),
  CONSTRAINT fk_users_sucursal FOREIGN KEY (id_sucursal) REFERENCES sucursales(id_sucursal)

) ENGINE = InnoDB;

CREATE TABLE sectores_economicos(

  id_sector int auto_increment not null,
  nombre_sector text,
  created_at datetime default null,
  updated_at datetime default null,
  CONSTRAINT pk_sector PRIMARY KEY (id_sector)

) ENGINE = InnoDB;

CREATE TABLE prospectos(

  id_prospecto  int auto_increment not null,
  nombre text,
  rfc text,
  calle text,
  no_ext text,
  no_int text,
  colonia text,
  estado text,
  cp text,
  pais text,
  telefono1 text,
  telefono2 text,
  telefono3 text,
  paginaweb text,
  twitter text,
  facebook text,
  comentarios text,
  id_vendedor int,
  id_sector int,
  status text,
  created_at datetime default null,
  updated_at datetime default null,
  CONSTRAINT pk_prospecto PRIMARY KEY (id_prospecto),
  CONSTRAINT fk_prospectos_vendedor FOREIGN KEY (id_vendedor) REFERENCES users(id_usuario),
  CONSTRAINT fk_prospectos_sector FOREIGN KEY (id_sector) REFERENCES sectores_economicos(id_sector)

) ENGINE = InnoDB;

CREATE TABLE contactos(

  id_contacto int auto_increment not null,
  id_prospecto int,
  nombre text,
  apellido text,
  telefono1 text,
  telefono2 text,
  email text,
  puesto text,
  comentario text,
  created_at datetime default null,
  updated_at datetime default null,
  CONSTRAINT pk_contacto PRIMARY KEY (id_contacto),
  CONSTRAINT fk_contactos_prospecto FOREIGN KEY (id_prospecto) REFERENCES prospectos(id_prospecto)

) ENGINE = InnoDB;

CREATE TABLE interacciones(
  id_inteaccion int auto_increment not null,
  id_prospecto int,
  accion int,
  comentario int,
  fecha_interaccion date,
  fecha_concluido date,
  created_at datetime default null,
  updated_at datetime default null,
  CONSTRAINT pk_iteraccion PRIMARY KEY (id_inteaccion),
  CONSTRAINT pk_interacciones_prospecto FOREIGN KEY (id_prospecto) REFERENCES prospectos(id_prospecto)
) ENGINE = InnoDB;

CREATE TABLE moneda(
  id_moneda int auto_increment not null,
  nombre_moneda text,
  CONSTRAINT pk_moneda PRIMARY KEY (id_moneda)
) ENGINE = InnoDB;

CREATE TABLE historico_tipos_cambios(
  id int auto_increment not null,
  id_moneda int,
  tipo_cambio float,
  created_at datetime default null,
  updated_at datetime default null,
  CONSTRAINT pk_id_tipo_cambio PRIMARY KEY (id),
  CONSTRAINT fk_idtipos_moneda FOREIGN KEY (id_moneda) REFERENCES moneda(id_moneda)

) ENGINE = InnoDB;

CREATE TABLE encabezado_documentos(

  id_documento int auto_increment not null,
  id_prospecto int,
  nombre_prospecto text,
  referencia text,
  id_moneda int,
  tipo_cambio float,
  subtotal float,
  iva float,
  importe float,
  comentarios text,
  status text,
  fechavencimiento date,
  created_at datetime default null,
  updated_at datetime default null,
  CONSTRAINT pk_documento PRIMARY KEY (id_documento),
  CONSTRAINT fk_documentos_prospecto FOREIGN KEY (id_prospecto) REFERENCES prospectos(id_prospecto),
  CONSTRAINT fk_documentos_moneda FOREIGN KEY (id_moneda) REFERENCES moneda(id_moneda)


) ENGINE = InnoDB;

CREATE TABLE tipos_maquinas(

  id_tipo int auto_increment not null,
  nombre_tipo text,
  created_at datetime default null,
  updated_at datetime default null,
  CONSTRAINT pk_tipo PRIMARY KEY (id_tipo)

) ENGINE = InnoDB;

CREATE TABLE maquinas(
  id_maquina int auto_increment not null,
  nombre text,
  modelo text,
  id_tipo int,
  capacidad float,
  motor text,
  anio int,
  foto_maquina text,
  observaciones text,
  id_moneda int,
  precio float,
  created_at datetime default null,
  updated_at datetime default null,
  CONSTRAINT pk_maquina PRIMARY KEY (id_maquina),
  CONSTRAINT fk_maquinas_tipo FOREIGN KEY (id_tipo) REFERENCES tipos_maquinas(id_tipo),
  CONSTRAINT fk_maquinas_moneda FOREIGN KEY (id_moneda) REFERENCES moneda(id_moneda)

) ENGINE = InnoDB;

CREATE TABLE detalle_documentos(

  id_detalle int auto_increment not null,
  id_documento int,
  consecutivo int,
  id_maquina int,
  cantidad int,
  precio_u float,
  observaciones text,
  created_at datetime default null,
  updated_at datetime default null,
  CONSTRAINT pk_detalle PRIMARY KEY (id_detalle),
  CONSTRAINT fk_detalles_documento FOREIGN KEY (id_documento) REFERENCES encabezado_documentos(id_documento),
  CONSTRAINT fk_detalles_maquina FOREIGN KEY (id_maquina) REFERENCES maquinas(id_maquina)

) ENGINE = InnoDB

CREATE TABLE aditamentos(

  id_aditamento int auto_increment not null,
  nombre_aditamento text,
  precio float,
  created_at datetime default null,
  updated_at datetime default null,
  CONSTRAINT pk_aditamento PRIMARY KEY (id_aditamento)

) ENGINE = InnoDB;

CREATE TABLE maquina_aditamentos(

  id int auto_increment not null,
  id_documento int,
  consecutivo_detalle int,
  consecutivo_aditamento int,
  id_maquina int,
  id_aditamento int,
  observaciones text,
  CONSTRAINT pk_maqadi PRIMARY KEY (id),
  CONSTRAINT fk_maqaditamentos_documento FOREIGN KEY (id_documento) REFERENCES detalle_documentos(id_documento),
  CONSTRAINT fk_maqaditamentos_maquina FOREIGN KEY (id_maquina) REFERENCES maquinas(id_maquina),
  CONSTRAINT fk_maqaditamentos_aditamento FOREIGN KEY (id_aditamento) REFERENCES aditamentos(id_aditamento)


) ENGINE = InnoDB;



