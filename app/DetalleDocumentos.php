<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleDocumentos extends Model
{
    //
    protected $table = 'detalle_documentos';

    public function maquina_aditamentos(){
        return $this->hasMany('App\MaquinaAditamentos');
    }

    public function encabezado_documento(){
        return $this->belongsTo('App\EncabezadoDocumentos');
    }

    public function maquina(){
        return $this->belongsTo('App\Maquinas');
    }
}
