<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prospectos extends Model
{
    //
    protected $table = "prospectos";

    public function user(){
        return $this->belongsTo('App\User','id_usuario');
    }

    public function sector_economico(){
        return $this->belongsTo('App\SectoresEconomicos','id_sector');
    }

    public function contactos(){
        return $this->hasMany('App\Contactos','id_prospecto','id_prospecto');
    }

    public function encabezado_documentos(){
        return $this->hasMany('App\EncabezadoDocumentos');
    }

    public function interacciones(){
        return $this->hasMany('App\Interacciones');
    }
}
