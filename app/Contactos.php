<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactos extends Model
{
    //
    protected $table = 'contactos';

    public function prospecto(){
        return $this->belongsTo('App\Prospectos','id_prospecto','id_prospecto');
    }
}
