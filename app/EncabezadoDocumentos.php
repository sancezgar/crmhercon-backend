<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EncabezadoDocumentos extends Model
{
    //
    protected $table = 'encabezado_documentos';

    public function prospecto(){
        return $this->belongsTo('App\Prospectos');
    }

    public function moneda(){
        return $this->belongsTo('App\Monedas');
    }

    public function detalle_documentos(){
        return $this->hasMany('App\DetalleDocumentos');
    }
}
