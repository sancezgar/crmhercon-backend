<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sucursales;
class SucursalController extends Controller
{

    public function __construct()
    {
        $this->middleware('api.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sucursales = Sucursales::all();

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'sucursales' => $sucursales
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $json = $request->input('json',null);

        $params = json_decode($json);

        $params_array = json_decode($json,true);

        if(!empty($params_array)){

            //validar los datos
            $validar = \Validator::make($params_array,[
                'razon_social' => 'required',
                'rfc_sucursal' => 'required|unique:App\Sucursales,rfc_sucursal',
                'alias' => 'required'
            ]);

            if($validar->fails()){
                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Los datos obtenidos no son correctos',
                    'errors' => $validar->errors()
                );
            }else{

                //guardar
                $sucursal = new Sucursales();
                $sucursal->razon_social = $params->razon_social;
                $sucursal->rfc_sucursal = $params->rfc_sucursal;
                $sucursal->alias = $params->alias;
                $sucursal -> save();

                $data = array(
                    'status' => 'success',
                    'code' => 201,
                    'message' => 'Los datos se guardaron correctamente',
                    'sucursales' => $sucursal
                );

            }

        }else{
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'No se enviaron datos'
            );
        }

        //enviar respuesta
        return response()->json($data,$data['code']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $sucursal = Sucursales::where('id_sucursal',$id)->get();


        if(count($sucursal) > 0){
            $data = [
              'status' => 'success',
              'code' => 200,
              'sucursal' => $sucursal[0]
            ];
        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No se ha encontrado la sucursal'
            ];
        }

        return response()->json($data,$data['code']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $json = $request->input('json',null);
        $params_array = json_decode($json,true);

        if(!empty($params_array)){
            // condiciones de validacion para los datos enviados
            $validar = \Validator::make($params_array,[
                'razon_social' => 'required',
                'rfc_sucursal' => [
                                    'required',
                                    'unique:App\Sucursales,rfc_sucursal,'.$id.',id_sucursal'
                                  ],
                'alias' => 'required'
            ]);
            // revisar la validación
            if($validar->fails()){

                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Datos erroneos',
                    'errors' => $validar->errors()
                ];

            }else{

                // Quitar campos que no son necesario actualizar
                unset($params_array['id_sucursal']);
                unset($params_array['created_at']);
                unset($params_array['updated_at']);

                // Actualizar datos
                Sucursales::where('id_sucursal',$id)->update($params_array);

                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Se actualizó la sucursal',
                    'sucursal' => Sucursales::where('id_sucursal',$id)->first()
                ];
            }
        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No se enviaron datos',
            ];
        }

        return response()->json($data,$data['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sucursal = Sucursales::where('id_sucursal',$id)->get();

        if(count($sucursal)>0){

            Sucursales::where('id_sucursal',$id)->delete();

            $data = [
                'status' => 'success',
                'code' => 200,
                'message' => 'Se elimino correctamente',
                'sucursal' => $sucursal[0]
            ];

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No existe esa sucursal, intentalo de nuevo',
            ];
        }

        return response()->json($data,$data['code']);

    }
}
