<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SectoresEconomicos;

class SectorEconomicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('api.auth');
    }

    public function index()
    {
        $sectores = SectoresEconomicos::all();

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'sectores' => $sectores
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Obtener los datos del formulario
        $json = $request->input('json',null);
        $paramas_array = json_decode($json,true);

        if(!empty($paramas_array)){

            //validar los datos
            $validar = \Validator::make($paramas_array,[
                'nombre_sector' => 'required',
            ]);

            if($validar->fails()){
                $data = [
                    'status'=>'error',
                    'code'=>400,
                    'message'=>'Datos erroneos',
                    'errors' => $validar->errors()
                ];
            }else{

                //guardar datos
                $sector = new SectoresEconomicos();
                $sector->nombre_sector = $paramas_array['nombre_sector'];
                $sector->save();

                $data = [
                    'status' => 'success',
                    'code' => 201,
                    'message' => 'Se guardó correctamente',
                    'sector' => $sector
                ];

            }

        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No hay datos'
            ];
        }

        return response()->json($data,$data['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Realizamos la busqueda
        $sector = SectoresEconomicos::where('id_sector',$id)->get();
        $valida = json_decode($sector,true);

        if(count($valida)>0){
            $data = [
                'status'=>'success',
                'code'=>200,
                'sector' => $sector
            ];
        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No se encontró el sector'
            ];
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtenemos los datos del Request
        $json = $request->input('json',null);
        $params = json_decode($json,true);

        if(!empty($params)){

            //validar datos
            $validar = \Validator::make($params,[
                'nombre_sector' => 'required'
            ]);

            if($validar->fails()){

                $data = [
                    'status'=>'error',
                    'code'=>400,
                    'message'=>'Datos erroneos',
                    'errors' => $validar->errors()
                ];

            }else{

                //validar que exista el sector
                $sector = SectoresEconomicos::where('id_sector',$id)->get();
                $valida = json_decode($sector,true);

                if(count($valida)>0){

                    SectoresEconomicos::where('id_sector',$id)->update($params);

                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Se actualizó correctamente',
                        'sector' => SectoresEconomicos::where('id_sector',$id)->get()
                    ];

                }else{

                    $data = [
                        'status'=>'error',
                        'code'=>400,
                        'message'=>'No se encontró el sector'
                    ];

                }

            }

        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No hay datos'
            ];
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sector = SectoresEconomicos::where('id_sector',$id)->get();
        $valida = json_decode($sector,true);

        if(count($valida)>0){
            SectoresEconomicos::where('id_sector',$id)->delete();

            $data = [
                'status' => 'success',
                'code' => 200,
                'message' => 'Se eliminó con exito',
                'sector' => $sector
            ];

        }else{

            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No se encontró el sector'
            ];

        }

        return response()->json($data,$data['code']);
    }
}
