<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Maquinas;

class MaquinaController extends Controller
{
    public function __construct()
    {
        $this->middleware('api.auth');
    }

    public function index()
    {
        $maquinas = Maquinas::all();

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'maquinas' => $maquinas
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Obtener los datos del formulario
        $json = $request->input('json',null);
        $paramas_array = json_decode($json,true);

        if(!empty($paramas_array)){

            //validar los datos
            $validar = \Validator::make($paramas_array,[
                'nombre' => 'required',
                'modelo' => 'required',
                'id_tipo' => 'required|integer',
                'anio' => 'required|integer',
                'id_moneda' => 'required|integer',
                'precio' => 'required|numeric'
            ]);

            if($validar->fails()){
                $data = [
                    'status'=>'error',
                    'code'=>400,
                    'message'=>'Datos erroneos',
                    'errors' => $validar->errors()
                ];
            }else{

                //guardar datos
                $maquina = new Maquinas();
                $maquina->nombre = $paramas_array['nombre'];
                $maquina->modelo = $paramas_array['modelo'];
                $maquina->id_tipo = $paramas_array['id_tipo'];
                $maquina->capacidad = $paramas_array['capacidad'];
                $maquina->motor = $paramas_array['motor'];
                $maquina->anio = $paramas_array['anio'];
                $maquina->foto_maquina = $paramas_array['foto_maquina'];
                $maquina->observaciones = $paramas_array['observaciones'];
                $maquina->id_moneda = $paramas_array['id_moneda'];
                $maquina->precio = $paramas_array['precio'];
                $maquina->save();

                $data = [
                    'status' => 'success',
                    'code' => 201,
                    'message' => 'Se guardó correctamente',
                    'maquina' => $maquina
                ];

            }

        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No hay datos'
            ];
        }

        return response()->json($data,$data['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Realizamos la busqueda
        $maquina = Maquinas::where('id_maquina',$id)->get();
        $valida = json_decode($maquina,true);

        if(count($valida)>0){
            $data = [
                'status'=>'success',
                'code'=>200,
                'maquina' => $maquina[0]
            ];
        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No se encontró la máquina'
            ];
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtenemos los datos del Request
        $json = $request->input('json',null);
        $params = json_decode($json,true);

        if(!empty($params)){

            //validar datos
            $validar = \Validator::make($params,[
                'nombre' => 'required',
                'modelo' => 'required',
                'id_tipo' => 'required|integer',
                'anio' => 'required|integer',
                'id_moneda' => 'required|integer',
                'precio' => 'required|numeric'
            ]);

            if($validar->fails()){

                $data = [
                    'status'=>'error',
                    'code'=>400,
                    'message'=>'Datos erroneos',
                    'errors' => $validar->errors()
                ];

            }else{

                // Quitar campos
                unset($params['id_maquina']);
                unset($params['created_at']);

                //validar que exista el maquina
                $maquina= Maquinas::where('id_maquina',$id)->get();
                $valida = json_decode($maquina,true);

                if(count($valida)>0){

                    Maquinas::where('id_maquina',$id)->update($params);

                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Se actualizó correctamente',
                        'maquina' => Maquinas::where('id_maquina',$id)->first()
                    ];

                }else{

                    $data = [
                        'status'=>'error',
                        'code'=>400,
                        'message'=>'No se encontró la máquina'
                    ];

                }

            }

        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No hay datos'
            ];
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $maquina = Maquinas::where('id_maquina',$id)->get();
        $valida = json_decode($maquina,true);

        if(count($valida)>0){
            Maquinas::where('id_maquina',$id)->delete();

            $data = [
                'status' => 'success',
                'code' => 200,
                'message' => 'Se eliminó la máquina con exito',
                'maquina' => $maquina[0]
            ];

        }else{

            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No se encontró la máquina'
            ];

        }

        return response()->json($data,$data['code']);
    }
}
