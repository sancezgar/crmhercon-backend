<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('api.auth',['except' => ['login','revisarToken']]);
    }

    public function index()
    {
        //
        $users = User::all();
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'users' => $users
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //obtener los datos del request
        $json = $request->input('json',null);
        $params = json_decode($json);
        $params_array = json_decode($json,true);

        if(!empty($params_array)){

            //limpiar los datos
            $params_array = array_map('trim',$params_array);

            //validar los datos
            $validar = \Validator::make($params_array,[
                'nombre_usuario' => 'required',
                'rol' => 'required',
                'email_usuario' => 'required|email|unique:App\User,email_usuario',
                'contrasena' => 'required',
                'id_sucursal' => 'required'
            ]);

            if($validar -> fails()){
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Datos incorrectos',
                    'errors' => $validar->errors()
                ];
            }else{

                //guardar los datos
                $user = new User();

                $user->nombre_usuario = $params_array['nombre_usuario'];
                $user->rol = $params_array['rol'];
                $user->email_usuario = $params_array['email_usuario'];
                $user->contrasena = $params->contrasena;
                $user->id_sucursal = $params_array['id_sucursal'];
                $user->save();

                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Usuario guardado',
                    'user' => $user
                ];

            }

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No hay datos'
            ];
        }
        // Enviar Resultado en formato JSON
        return response()->json($data,$data['code']);

    }

    public function login(Request $request){

        $jwtAuth = new \JWTAuth;

        //Recibir los datos por Post
        $json = $request->input('json',null);
        $params = json_decode($json);
        $params_array = json_decode($json,true);

        if(!empty($params_array)){

            //validar datos
            $validar = \Validator::make($params_array,[
              'email_usuario' => 'required|email',
              'contrasena' => 'required'
            ]);

            if($validar->fails()){
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Datos erroneos',
                    'errors' => $validar->errors()
                ];
            }
            else{

                $signup = $jwtAuth->signup($params->email_usuario,$params->contrasena);

                // Devolver token o datos
                if(!empty($params->getToken)){
                    $signup = $jwtAuth->signup( $params->email_usuario,$params->contrasena,true );
                }

                return response()->json($signup,200);

            }

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No hay datos'
            ];
        }

        // Envía Resultado en formato JSON
        return response()->json($data,$data['code']);
    }

    public function revisarToken(Request $request){

        $token = $request->input('token',null);


        if(!empty($token)){

            $jwtAuth = new \JWTAuth();
            $checkToken = $jwtAuth->checkToken($token);

        }else{

            $checkToken = false;

        }

        return response()->json([
            'sesion' => $checkToken
        ], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::where('id_usuario',$id)->get();

        // El resultado lo convertimos en array para asegurarnos que exista
        $valida = json_decode($user,true);

        if(count($valida)>0){
            $data = [
                'status' => 'success',
                'code' => 200,
                'user' => $user
            ];
        }else{

            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No se encontró el usuario'
            ];

        }
        // Envía Resultado en formato JSON
        return response()->json($data,$data['code']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtener los datos del formulario
        $json = $request->input('json',null);
        $params_array = json_decode($json,true);

        if(!empty($params_array)){

            // validar datos
            $validar = \Validator::make($params_array,[
                'nombre_usuario' => 'required',
                'rol' => 'required',
                'email_usuario' => 'required|email|unique:App\User,email_usuario,'.$id.',id_usuario',
                'contrasena' => 'required',
                'id_sucursal' => 'required'
            ]);

            if($validar -> fails()){

                $data = [
                    'status'=>'error',
                    'code' => 400,
                    'message' => 'Son erroneos los datos',
                    'erros' => $validar->errors()
                ];

            }else{

                // Quitar campos no necesarios
                unset($params_array['id_usuario']);
                unset($params_array['created_at']);

                // Comprobar que exista el usuario a editar
                $user = User::where('id_usuario',$id)->get();

                $valida = json_decode($user,true);

                if(count($valida)>0){

                    User::where('id_usuario',$id) -> update($params_array);

                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Usuario actualizado',
                        'user' => User::where('id_usuario',$id)->get()
                    ];

                }else{
                    $data = [
                        'status'=>'error',
                        'code' => 400,
                        'message' => 'No existe ese usuario'
                    ];
                }

            }

        }else{

            $data = [
                'status'=>'error',
                'code' => 400,
                'message' => 'No hay datos que actualizar'
            ];

        }
        return response()->json($data,$data['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::where('id_usuario',$id)->get();

        $valida = json_decode($user,true);

        if(count($valida)>0){

            User::where('id_usuario',$id)->delete();
            $data = [
                'status' => 'success',
                'code' => 200,
                'message' => 'Usuario eliminado',
                'user' => $user
            ];

        }
        else{
            $data = [
                'status'=>'error',
                'code' => 400,
                'message' => 'No existe ese usuario'
            ];
        }

        return response()->json($data,$data['code']);
    }
}
