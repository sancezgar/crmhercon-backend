<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Acciones;

class AccionController extends Controller
{

    public function __construct()
    {
        $this->middleware('api.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $acciones = Acciones::all();
        return response()->json([
            'status'=>'success',
            'code' => 200,
            'acciones' => $acciones
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $json = $request->input('json',null);
        $params = json_decode($json,true);

        if(!empty($params)){

            //validar datos
            $valida = \Validator::make($params,[
               'nombre_accion' => 'required'
            ]);

            if($valida->fails()){
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Datos erroneos',
                    'errors' => $valida->errors()
                ];
            }else{

                $accion = new Acciones();
                $accion->nombre_accion = $params['nombre_accion'];
                $accion->save();

                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'La accion se guardó con exito',
                    'accion' => $accion
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No hay datos'
            ];

        }

        return response()->json($data,$data['code']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $accion = Acciones::where('id_accion',$id)->get();

        if(count($accion)>0){
            $data = [
                'status' => 'success',
                'code' => 200,
                'accion' => $accion[0]
            ];
        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No existe esa acción'
            ];
        }
        return response()->json($data,$data['code']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $accion = Acciones::where('id_accion',$id)->get();

        if(count($accion)>0){

            $json = $request->input('json',null);
            $params = json_decode($json,true);

            if(!empty($params)){

                //validar datos
                $valida = \Validator::make($params,[
                   'nombre_accion' => 'required'
                ]);

                if($valida->fails()){

                    $data = [
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Datos erroneos',
                        'errors' => $valida->errors()
                    ];

                }else{

                    //eliminamos datos no necesarios
                    unset($params['id_accion']);
                    unset($params['created_at']);

                    Acciones::where('id_accion',$id)->update($params);

                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Accion actulizado',
                        'accion' => Acciones::where('id_accion',$id)->first()
                    ];

                }

            }else{
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'No hay datos'
                ];
            }

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No existe esa acción'
            ];
        }

        return response()->json($data,$data['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $accion = Acciones::where('id_accion',$id)->get();

        if(count($accion)>0){
            Acciones::where('id_accion',$id)->delete();
            $data = [
                'status' => 'success',
                'code' => 200,
                'message' => 'Se eliminó la accion correctamente',
                'accion' => $accion[0]
            ];
        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No existe esa acción'
            ];
        }

        return response()->json($data,$data['code']);
    }
}
