<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interacciones;
use Symfony\Component\VarDumper\VarDumper;

class InteraccionController extends Controller
{
    public function __construct(){
        $this->middleware('api.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $interacciones = Interacciones::all();

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'interaciones' => $interacciones
        ],200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $json = $request->input('json',null);
        $params = json_decode($json,true);

        if(!empty($params)){

            //Válidamos los datos
            $validar = \Validator::make($params,[
                'id_prospecto' => 'required|integer',
                'id_accion' => 'required|integer',
                'fecha_interaccion' => 'required',
                'fecha_concluido' => 'required'
            ]);

            if($validar->fails()){

                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Datos erroneos',
                    'errors' => $validar->errors()
                ];

            }else{

                $interaccion = new Interacciones();
                $interaccion->id_accion = $params['id_accion'];
                $interaccion->id_prospecto = $params['id_prospecto'];
                $interaccion->comentario = $params['comentario'];
                $interaccion->fecha_interaccion = $params['fecha_interaccion'];
                $interaccion->fecha_concluido = $params['fecha_concluido'];
                $interaccion->save();

                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Interaccion guardada',
                    'interaccion' => $interaccion
                ];

            }

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No hay datos'
            ];
        }

        return response()->json($data,$data['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Mostrar un contacto en específico
        $interaccion = Interacciones::where('id_interaccion',$id)->get();
        if(count($interaccion)>0){

            $data = [
                'status' => 'success',
                'code' => 200,
                'interaccion' => $interaccion[0]
            ];

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No existe ese contacto'
            ];
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Buscar si existe el contacto a actualizar
        $interaccion = Interacciones::where('id_interaccion',$id)->get();
        if(count($interaccion)>0){

            $json = $request->input('json',null);
            $params = json_decode($json,true);

            if(!empty($params)){
                //Válidamos los datos
                $validar = \Validator::make($params,[
                    'id_prospecto' => 'required|integer',
                    'id_accion' => 'required|integer',
                    'fecha_interaccion' => 'required',
                    'fecha_concluido' => 'required'
                ]);

                if($validar->fails()){

                    $data = [
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Datos erroneos',
                        'errors' => $validar->errors()
                    ];

                }else{
                    // Quitar datos para no actualizar
                    unset($params['id_interaccion']);
                    unset($params['created_at']);

                    //Acutalizar los datos
                    Interacciones::where('id_interaccion',$id)->update($params);

                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Se actualizó los datos de la interacción',
                        'contacto' => Interacciones::where('id_interaccion',$id)->first()
                    ];
                }
            }else{
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'No hay datos'
                ];
            }

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No existe esa interacción'
            ];
        }

        return response()->json($data,$data['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Buscamos si existe el contacto para eliminar
        //Mostrar un contacto en especifico
        $interaccion = Interacciones::where('id_interaccion',$id)->get();
        if(count($interaccion)>0){

            Interacciones::where('id_interaccion',$id)->delete();

            $data = [
                'status' => 'success',
                'code' => 200,
                'message' => 'Se ha eliminado la interacción exitosamente',
                'interaccion' => $interaccion[0]
            ];

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No existe esa interacción'
            ];
        }

        return response()->json($data,$data['code']);

    }
}
