<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TiposMaquinas;

class TipoMaquinaController extends Controller
{
    public function __construct()
    {
        $this->middleware('api.auth');
    }

    public function index()
    {
        $tipo = TiposMaquinas::all();

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'tipos' => $tipo
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Obtener los datos del formulario
        $json = $request->input('json',null);
        $paramas_array = json_decode($json,true);

        if(!empty($paramas_array)){

            //validar los datos
            $validar = \Validator::make($paramas_array,[
                'nombre_tipo' => 'required',
            ]);

            if($validar->fails()){
                $data = [
                    'status'=>'error',
                    'code'=>400,
                    'message'=>'Datos erroneos',
                    'errors' => $validar->errors()
                ];
            }else{

                //guardar datos
                $tipo = new TiposMaquinas();
                $tipo->nombre_tipo = $paramas_array['nombre_tipo'];
                $tipo->save();

                $data = [
                    'status' => 'success',
                    'code' => 201,
                    'message' => 'Se guardó correctamente',
                    'tipo' => $tipo
                ];

            }

        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No hay datos'
            ];
        }

        return response()->json($data,$data['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Realizamos la busqueda
        $tipo = TiposMaquinas::where('id_tipo',$id)->get();
        $valida = json_decode($tipo,true);

        if(count($valida)>0){
            $data = [
                'status'=>'success',
                'code'=>200,
                'tipo' => $tipo
            ];
        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No se encontró el tipo de máquina'
            ];
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtenemos los datos del Request
        $json = $request->input('json',null);
        $params = json_decode($json,true);

        if(!empty($params)){

            //validar datos
            $validar = \Validator::make($params,[
                'nombre_tipo' => 'required'
            ]);

            if($validar->fails()){

                $data = [
                    'status'=>'error',
                    'code'=>400,
                    'message'=>'Datos erroneos',
                    'errors' => $validar->errors()
                ];

            }else{

                // Quitar campos
                unset($params['id_tipo']);
                unset($params['created_at']);

                //validar que exista el tipo
                $tipo= TiposMaquinas::where('id_tipo',$id)->get();
                $valida = json_decode($tipo,true);

                if(count($valida)>0){

                    TiposMaquinas::where('id_tipo',$id)->update($params);

                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Se actualizó correctamente',
                        'tipo' => TiposMaquinas::where('id_tipo',$id)->get()
                    ];

                }else{

                    $data = [
                        'status'=>'error',
                        'code'=>400,
                        'message'=>'No se encontró el tipo de máquina'
                    ];

                }

            }

        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No hay datos'
            ];
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tipo = TiposMaquinas::where('id_tipo',$id)->get();
        $valida = json_decode($tipo,true);

        if(count($valida)>0){
            TiposMaquinas::where('id_tipo',$id)->delete();

            $data = [
                'status' => 'success',
                'code' => 200,
                'message' => 'Se eliminó el tipo de máquina con exito',
                'tipo' => $tipo
            ];

        }else{

            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No se encontró el tipo de máquina'
            ];

        }

        return response()->json($data,$data['code']);
    }
}
