<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prospectos;
use App\Contactos;

class ProspectoController extends Controller
{
    public function __construct()
    {
        $this->middleware('api.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Obtenemos todos los prospectos
        $prospectos = Prospectos::all()->load('contactos');
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'prospectos' => $prospectos
        ],200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Obtener los datos del request
        $json = $request->input('json',null);
        $params = json_decode($json,true);

        if(!empty($params)){

            //Válidamos los datos
            $validar = \Validator::make($params,[
                'nombre' => 'required',
                'calle' => 'required',
                'colonia' => 'required',
                'estado' => 'required',
                'cp' => 'required',
                'pais' => 'required',
                'telefono1' => 'required',
                'id_vendedor' => 'required|integer',
                'id_sector' => 'required|integer',
                'activo' => 'required|integer'
            ]);

            if($validar->fails()){

                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Datos erroneos',
                    'errors' => $validar->errors()
                ];

            }else{

                $prospecto = new Prospectos();
                $prospecto->nombre = $params['nombre'];
                $prospecto->rfc = $params['rfc'];
                $prospecto->calle = $params['calle'];
                $prospecto->no_ext = $params['no_ext'];
                $prospecto->no_int = $params['no_int'];
                $prospecto->colonia = $params['colonia'];
                $prospecto->estado = $params['estado'];
                $prospecto->cp = $params['cp'];
                $prospecto->pais = $params['pais'];
                $prospecto->telefono1 = $params['telefono1'];
                $prospecto->telefono2 = $params['telefono2'];
                $prospecto->telefono3 = $params['telefono3'];
                $prospecto->paginaweb = $params['paginaweb'];
                $prospecto->twitter = $params['twitter'];
                $prospecto->facebook = $params['facebook'];
                $prospecto->comentarios = $params['comentarios'];
                $prospecto->id_vendedor = $params['id_vendedor'];
                $prospecto->id_sector = $params['id_sector'];
                $prospecto->status = $params['status'];
                $prospecto->activo = $params['activo'];
                $prospecto->save();

                $messageContact = [];

                foreach ($params['contactos'] as $key=>$value){
                    $messageContact[$key] = $this->agregarContactos($value,$prospecto->id);
                }

                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Prospecto guardado',
                    'prospecto' => $prospecto,
                    'contactos' => $messageContact
                ];



            }

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No hay datos'
            ];
        }

        return response()->json($data,$data['code']);
    }

    public function agregarContactos($params,$id){

        if(!empty($params)){

            //Válidamos los datos
            $validar = \Validator::make($params,[
                'nombre' => 'required',
                'apellido' => 'required',
                'email' => 'required',
                'activo' => 'required|integer'
            ]);

            if($validar->fails()){

                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Datos erroneos',
                    'errors' => $validar->errors()
                ];

            }else{

                $contacto = new Contactos();
                $contacto->nombre = $params['nombre'];
                $contacto->id_prospecto = $id;
                $contacto->apellido = $params['apellido'];
                $contacto->telefono1 = $params['telefono1'];
                $contacto->telefono2 = $params['telefono2'];
                $contacto->email = $params['email'];
                $contacto->puesto = $params['puesto'];
                $contacto->comentario = $params['comentario'];
                $contacto->activo = $params['activo'];
                $contacto->save();

                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Contacto guardado',
                    'contacto' => $contacto
                ];

            }

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No hay datos'
            ];
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $prospecto = Prospectos::where('id_prospecto',$id)->get()->load('contactos');
        $valida = json_decode($prospecto,true);

        if(count($valida)>0){

            $data = [
                'status' => 'success',
                'code' => 200,
                'prospecto' => $prospecto[0]
            ];

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No se encontró el prospecto'
            ];
        }
        return response()->json($data,$data['code']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //obtener los datos
        $prospecto = Prospectos::where('id_prospecto',$id)->get();
        $valida = json_decode($prospecto,true);

        if(count($valida)>0){

            $json = $request->input('json',null);
            $params = json_decode($json,true);

            if(!empty($params)) {

                //Válidamos los datos
                $validar = \Validator::make($params, [
                    'nombre' => 'required',
                    'calle' => 'required',
                    'colonia' => 'required',
                    'estado' => 'required',
                    'cp' => 'required',
                    'pais' => 'required',
                    'telefono1' => 'required',
                    'id_vendedor' => 'required|integer',
                    'id_sector' => 'required|integer',
                    'activo' => 'required|integer'
                ]);

                if($validar->fails()){

                    $data = [
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Datos erroneos',
                        'errors' => $validar->errors()
                    ];

                }else{

                    // Quitar campos inecesarios
                    unset($params['contactos']);
                    unset($params['id_prospecto']);
                    unset($params['created_at']);

                    // Actualizar los datos

                    Prospectos::where('id_prospecto',$id)->update($params);

                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Se actualizó el prospecto',
                        'errors' => Prospectos::where('id_prospecto',$id)->first()->load('contactos')
                    ];

                }



            }else{
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'No hay datos'
                ];
            }

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No se encontró el prospecto'
            ];
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO: Realizar el proceso para eliminar el prospecto y ver el comportamiento de los contactos
        // validar si existe el prospecto
        $prospecto = Prospectos::where('id_prospecto',$id)->get();

        if(count($prospecto)>0){
            Contactos::where('id_prospecto',$id)->delete();
            Prospectos::where('id_prospecto',$id)->delete();

            $data = [
                'status' => 'success',
                'code' => 200,
                'message' => 'Se eliminó el prospecto satisfactoriamente',
                'prospecto' => $prospecto
            ];

        }else{
            $data = [
                'status' => 'success',
                'code' => 200,
                'message' => 'El prospecto no existe',
            ];
        }

        return response()->json($data,$data['code']);
    }
}
