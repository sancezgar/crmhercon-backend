<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Monedas;

class MonedaController extends Controller
{
    public function __construct()
    {
        $this->middleware('api.auth');
    }

    public function index()
    {
        $monedas = Monedas::all();

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'sectores' => $monedas
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Obtener los datos del formulario
        $json = $request->input('json',null);
        $paramas_array = json_decode($json,true);

        if(!empty($paramas_array)){

            //validar los datos
            $validar = \Validator::make($paramas_array,[
                'nombre_moneda' => 'required',
            ]);

            if($validar->fails()){
                $data = [
                    'status'=>'error',
                    'code'=>400,
                    'message'=>'Datos erroneos',
                    'errors' => $validar->errors()
                ];
            }else{

                //guardar datos
                $moneda = new Monedas();
                $moneda->nombre_moneda = $paramas_array['nombre_moneda'];
                $moneda->save();

                $data = [
                    'status' => 'success',
                    'code' => 201,
                    'message' => 'Se guardó correctamente',
                    'sector' => $moneda
                ];

            }

        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No hay datos'
            ];
        }

        return response()->json($data,$data['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Realizamos la busqueda
        $moneda = Monedas::where('id_moneda',$id)->get();
        $valida = json_decode($moneda,true);

        if(count($valida)>0){
            $data = [
                'status'=>'success',
                'code'=>200,
                'sector' => $moneda
            ];
        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No se encontró la moneda'
            ];
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtenemos los datos del Request
        $json = $request->input('json',null);
        $params = json_decode($json,true);

        if(!empty($params)){

            //validar datos
            $validar = \Validator::make($params,[
                'nombre_moneda' => 'required'
            ]);

            if($validar->fails()){

                $data = [
                    'status'=>'error',
                    'code'=>400,
                    'message'=>'Datos erroneos',
                    'errors' => $validar->errors()
                ];

            }else{

                // Quitar campos
                unset($params['id_moneda']);
                unset($params['created_at']);

                //validar que exista el sector
                $moneda = Monedas::where('id_moneda',$id)->get();
                $valida = json_decode($moneda,true);

                if(count($valida)>0){

                    Monedas::where('id_moneda',$id)->update($params);

                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Se actualizó correctamente',
                        'sector' => Monedas::where('id_moneda',$id)->get()
                    ];

                }else{

                    $data = [
                        'status'=>'error',
                        'code'=>400,
                        'message'=>'No se encontró la moneda'
                    ];

                }

            }

        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No hay datos'
            ];
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $moneda = Monedas::where('id_moneda',$id)->get();
        $valida = json_decode($moneda,true);

        if(count($valida)>0){
            Monedas::where('id_moneda',$id)->delete();

            $data = [
                'status' => 'success',
                'code' => 200,
                'message' => 'Se eliminó la moneda con exito',
                'sector' => $moneda
            ];

        }else{

            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No se encontró la moneda'
            ];

        }

        return response()->json($data,$data['code']);
    }
}
