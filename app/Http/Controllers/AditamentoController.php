<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aditamentos;

class AditamentoController extends Controller
{
    public function __construct()
    {
        $this->middleware('api.auth');
    }

    public function index()
    {
        $aditamentos = Aditamentos::all();

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'aditamentos' => $aditamentos
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Obtener los datos del formulario
        $json = $request->input('json',null);
        $paramas_array = json_decode($json,true);

        if(!empty($paramas_array)){

            //validar los datos
            $validar = \Validator::make($paramas_array,[
                'nombre_aditamento' => 'required',
                'precio' => 'required',
            ]);

            if($validar->fails()){
                $data = [
                    'status'=>'error',
                    'code'=>400,
                    'message'=>'Datos erroneos',
                    'errors' => $validar->errors()
                ];
            }else{

                //guardar datos
                $aditamento = new Aditamentos();
                $aditamento->nombre_aditamento = $paramas_array['nombre_aditamento'];
                $aditamento->precio = $paramas_array['precio'];
                $aditamento->save();

                $data = [
                    'status' => 'success',
                    'code' => 201,
                    'message' => 'Se guardó correctamente',
                    'aditamento' => $aditamento
                ];

            }

        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No hay datos'
            ];
        }

        return response()->json($data,$data['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Realizamos la busqueda
        $aditamento = Aditamentos::where('id_aditamento',$id)->get();
        $valida = json_decode($aditamento,true);

        if(count($valida)>0){
            $data = [
                'status'=>'success',
                'code'=>200,
                'aditamento' => $aditamento[0]
            ];
        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No se encontró el aditamento'
            ];
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtenemos los datos del Request
        $json = $request->input('json',null);
        $params = json_decode($json,true);

        if(!empty($params)){

            //validar datos
            $validar = \Validator::make($params,[
                'nombre_aditamento' => 'required',
                'precio' => 'required'
            ]);

            if($validar->fails()){

                $data = [
                    'status'=>'error',
                    'code'=>400,
                    'message'=>'Datos erroneos',
                    'errors' => $validar->errors()
                ];

            }else{

                // Quitar campos
                unset($params['id_aditamento']);
                unset($params['created_at']);

                //validar que exista el aditamento
                $aditamento= Aditamentos::where('id_aditamento',$id)->get();
                $valida = json_decode($aditamento,true);

                if(count($valida)>0){

                    Aditamentos::where('id_aditamento',$id)->update($params);

                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Se actualizó correctamente',
                        'aditamento' => Aditamentos::where('id_aditamento',$id)->first()
                    ];

                }else{

                    $data = [
                        'status'=>'error',
                        'code'=>400,
                        'message'=>'No se encontró el aditamento'
                    ];

                }

            }

        }else{
            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No hay datos'
            ];
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $aditamento = Aditamentos::where('id_aditamento',$id)->get();
        $valida = json_decode($aditamento,true);

        if(count($valida)>0){
            Aditamentos::where('id_aditamento',$id)->delete();

            $data = [
                'status' => 'success',
                'code' => 200,
                'message' => 'Se eliminó el aditamento con exito',
                'aditamento' => $aditamento[0]
            ];

        }else{

            $data = [
                'status'=>'error',
                'code'=>400,
                'message'=>'No se encontró el aditamento'
            ];

        }

        return response()->json($data,$data['code']);
    }
}
