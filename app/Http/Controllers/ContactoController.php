<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contactos;

class ContactoController extends Controller
{
    public function __construct(){
        $this->middleware('api.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $contactos = Contactos::all();

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'contactos' => $contactos
        ],200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $json = $request->input('json',null);
        $params = json_decode($json,true);

        if(!empty($params)){

            //Válidamos los datos
            $validar = \Validator::make($params,[
                'nombre' => 'required',
                'apellido' => 'required',
                'email' => 'required',
                'activo' => 'required|integer'
            ]);

            if($validar->fails()){

                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Datos erroneos',
                    'errors' => $validar->errors()
                ];

            }else{

                $contacto = new Contactos();
                $contacto->nombre = $params['nombre'];
                $contacto->id_prospecto = $params['id_prospecto'];
                $contacto->apellido = $params['apellido'];
                $contacto->telefono1 = $params['telefono1'];
                $contacto->telefono2 = $params['telefono2'];
                $contacto->email = $params['email'];
                $contacto->puesto = $params['puesto'];
                $contacto->comentario = $params['comentario'];
                $contacto->activo = $params['activo'];
                $contacto->save();

                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Contacto guardado',
                    'contacto' => $contacto
                ];

            }

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No hay datos'
            ];
        }

        return response()->json($data,$data['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Mostrar un contacto en específico
        $contacto = Contactos::where('id_contacto',$id)->first();
        if(!empty($contacto)){

            $data = [
                'status' => 'success',
                'code' => 200,
                'contacto' => $contacto
            ];

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No existe ese contacto'
            ];
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Buscar si existe el contacto a actualizar
        $contacto = Contactos::where('id_contacto',$id)->get();
        if(count($contacto)>0){

            $json = $request->input('json',null);
            $params = json_decode($json,true);

            if(!empty($params)){
                //Válidamos los datos
                $validar = \Validator::make($params,[
                    'nombre' => 'required',
                    'apellido' => 'required',
                    'email' => 'required',
                    'activo' => 'required|integer'
                ]);

                if($validar->fails()){

                    $data = [
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Datos erroneos',
                        'errors' => $validar->errors()
                    ];

                }else{
                    // Quitar datos para no actualizar
                    unset($params['id_contacto']);
                    unset($params['created_at']);

                    //Acutalizar los datos
                    Contactos::where('id_contacto',$id)->update($params);

                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Se actualizó los datos del contacto',
                        'contacto' => Contactos::where('id_contacto',$id)->first()
                    ];
                }
            }else{
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'No hay datos'
                ];
            }

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No existe ese contacto'
            ];
        }

        return response()->json($data,$data['code']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Buscamos si existe el contacto para eliminar
        //Mostrar un contacto en especifico
        $contacto = Contactos::where('id_contacto',$id)->get();
        if(count($contacto)>0){

            Contactos::where('id_contacto',$id)->delete();

            $data = [
                'status' => 'success',
                'code' => 200,
                'message' => 'Se ha eliminado el contacto exitosamente',
                'contacto' => $contacto[0]
            ];

        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No existe ese contacto'
            ];
        }

        return response()->json($data,$data['code']);

    }
}
