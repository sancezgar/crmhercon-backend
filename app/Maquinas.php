<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maquinas extends Model
{
    //
    protected $table = 'maquinas';

    public function detalle_documento(){
        $this->hasMany('App\DetalleDocumentos');
    }

    public function moneda(){
        $this->belongsTo('App\Monedas');
    }

    public function tipo_maquina(){
        $this->belongsTo('App\tiposMaquinas');
    }
}
