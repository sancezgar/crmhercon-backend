<?php

namespace App\Helpers;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use App\User;

class JwtAuth{

    public $key;

    public function __construct()
    {
        $this->key = 'pXx9IcLbR1D3nqwPBXYoIp03AjQu0t';
    }

    public function signup($email, $contrasena, $getToken = null){

        $user = User::where([
            'email_usuario' => $email,
            'contrasena' => $contrasena
        ])->first();

        $sigup = false;

        if(is_object($user)){
            $sigup = true;
        }

        if($sigup) {

            $token = array(
                "sub" => $user->id_usuario,
                "name" => $user->nombre_usuario,
                "rol" => $user->rol,
                "email" => $user->email_usuario,
                "sucursal" => $user->id_sucursal,
                "id_usuario" => $user->id_usuario,
                "iat" => time(),
                "exp" => time() + (7 * 24 * 60 * 60),
            );
            $jwt = JWT::encode($token, $this->key, 'HS256');
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);

            if (is_null($getToken)) {
                $data = $jwt;
            } else {
                $data = $decoded;
            }
        }else{

            $data = array(
              'status' => 'error',
              'message' => 'Login incorrecto'
            );

        }

        return $data;

    }

    public function checkToken($jwt,$getIdentity = false){

        $auth = false;

        try{
            $jwt = str_replace('"','',$jwt);
            $decoded = JWT::decode($jwt,$this->key,['HS256']);
        }catch(\UnexpectedValueException $e){
            $auth = false;
        }catch(\DomainException $e){
            $auth = false;
        }

        if(!empty($decoded) && is_object($decoded) && isset($decoded->sub)){
            $auth = true;
        }else{
            $auth = false;
        }

        if($getIdentity){
            return $decoded;
        }

        return $auth;

    }

}