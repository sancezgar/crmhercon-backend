<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaquinaAditamentos extends Model
{
    //
    protected $table = 'maquina_aditamentos';

    public function detalle_documento(){
        $this->belongsTo('App\DetalleDocumentos');
    }

    public function aditamento(){
        $this->belongsTo('App\Aditamentos');
    }
}
