<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursales extends Model
{
    //
    protected $table = 'sucursales';

    public function users(){
        $this->hasMany('App\User');
    }
}
