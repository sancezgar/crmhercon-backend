<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interacciones extends Model
{
    //
    protected $table = 'interacciones';

    public function prospecto(){
        return $this->belongsTo('App\Prospectos');
    }

    public function accion(){
        return $this->belongsTo('App\Acciones','id_accion','id_accion');
    }
}
