<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monedas extends Model
{
    //
    protected $table = 'moneda';

    public function encabezado_documentos(){
        $this->hasMany('App\EncabezadoDocumentos');
    }

    public function historico_tipos_cambios(){
        $this->hasMany('App\HistoricoTiposCambios');
    }

    public function maquinas(){
        $this->hasMany('App\Maquinas');
    }
}
