<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectoresEconomicos extends Model
{
    //
    protected $table = "sectores_economicos";

    public function prospectos(){
        $this->hasMany('App\Prospectos');
    }
}
