<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acciones extends Model
{
    //
    protected $table = "acciones";

    public function interacciones(){

        return $this->hasMany("App\Interacciones",'id_accion','id_accion');
    }
}
