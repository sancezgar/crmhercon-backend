<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposMaquinas extends Model
{
    //
    protected $table = 'tipos_maquinas';

    public function maquinas(){
        $this->hasMany('App\Maquinas');
    }
}
