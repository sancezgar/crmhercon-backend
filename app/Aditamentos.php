<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aditamentos extends Model
{
    //
    protected $table = 'aditamentos';

    public function maquina_aditamentos(){
        return $this->hasMany('App\MaquinaAditamentos');
    }
}
