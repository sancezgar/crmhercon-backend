<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoTiposCambios extends Model
{
    //
    protected $table = 'historico_tipos_cambios';

    public function moneda(){
        $this->belongsTo('App\Monedas');
    }
}
